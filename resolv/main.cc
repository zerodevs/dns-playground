#include <cstdlib>
#include <netinet/in.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <glib.h>

static char *janus_stun_server = NULL;

typedef struct janus_network_address {
	/*!
	 * Should be either \c AF_INET for IPv4 or \c AF_INET6 for IPv6.
	 */
	int family;
	union {
		struct in_addr ipv4;
		struct in6_addr ipv6;
	};
} janus_network_address;

typedef struct janus_network_address_string_buffer {
	/*!
	 * Should be either \c AF_INET for IPv4 or \c AF_INET6 for IPv6.
	 */
	int family;
	union {
		char ipv4[INET_ADDRSTRLEN];
		char ipv6[INET6_ADDRSTRLEN];
	};
} janus_network_address_string_buffer;

int janus_network_address_is_null(const janus_network_address *a) {
	return !a || a->family == AF_UNSPEC;
}

void janus_network_address_string_buffer_nullify(janus_network_address_string_buffer *b) {
	if(b) {
		memset(b, '\0', sizeof(janus_network_address_string_buffer));
		b->family = AF_UNSPEC;
	}
}

int janus_network_address_to_string_buffer(const janus_network_address *a, janus_network_address_string_buffer *buf) {
	if(buf && !janus_network_address_is_null(a)) {
		janus_network_address_string_buffer_nullify(buf);
		buf->family = a->family;
		if(a->family == AF_INET) {
			return inet_ntop(AF_INET, &a->ipv4, buf->ipv4, INET_ADDRSTRLEN) ? 0 : -errno;
		} else {
			return inet_ntop(AF_INET6, &a->ipv6, buf->ipv6, INET6_ADDRSTRLEN) ? 0 : -errno;
		}
	} else {
		return -EINVAL;
	}
}


int janus_network_address_from_sockaddr(struct sockaddr *s, janus_network_address *a) {
	if(!s || !a)
		return -EINVAL;
	if(s->sa_family == AF_INET) {
		a->family = AF_INET;
		struct sockaddr_in *addr = (struct sockaddr_in *)s;
		a->ipv4 = addr->sin_addr;
		return 0;
	} else if(s->sa_family == AF_INET6) {
		a->family = AF_INET6;
		struct sockaddr_in6 *addr = (struct sockaddr_in6 *)s;
		a->ipv6 = addr->sin6_addr;
		return 0;
	}
	return -EINVAL;
}


int janus_network_address_string_buffer_is_null(const janus_network_address_string_buffer *b) {
	return !b || b->family == AF_UNSPEC;
}

const char *janus_network_address_string_from_buffer(const janus_network_address_string_buffer *b) {
	if(janus_network_address_string_buffer_is_null(b)) {
		return NULL;
	} else {
		return b->family == AF_INET ? b->ipv4 : b->ipv6;
	}
}


int main() {

  // list of alternative
  struct addrinfo *result;
  // cursor
  struct addrinfo *cursor;
  int s;

  s = getaddrinfo("stun1.l.google.com", NULL, NULL, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    exit(EXIT_FAILURE);
  }

  janus_network_address janus_addr;
  janus_network_address_string_buffer buffer;

  int err = janus_network_address_from_sockaddr(result->ai_addr, &janus_addr);

  if (err != 0) {
    printf("janus_network_address_from_sockaddr error");
    exit(EXIT_FAILURE);
  }

  err = janus_network_address_to_string_buffer(&janus_addr, &buffer);

  if (err != 0) {
    printf("janus_network_address_to_string_buffer error");
    exit(EXIT_FAILURE);
  }

  janus_stun_server = g_strdup(janus_network_address_string_from_buffer(&buffer));

  printf("janus stun server: %s\n", janus_stun_server);

  for (cursor = result; cursor != NULL; cursor = cursor->ai_next) {
    char address[INET_ADDRSTRLEN];
    struct sockaddr_in *addr = (struct sockaddr_in *)cursor->ai_addr;
    inet_ntop(AF_INET, &(addr->sin_addr), address, INET_ADDRSTRLEN);
    printf("IP ADDRESS: %s\n", address);
  }

  freeaddrinfo(result);
  return 0;
}
