package main

import (
	"flag"
	"fmt"

	"github.com/rs/zerolog/log"
	"gortc.io/stun"
)

var (
	addr     string
	protocol string
)

func init() {
	flag.StringVar(&protocol, "protocol", "udp", "address of stun server")
	flag.StringVar(&addr, "addr", "127.0.0.1:3478", "address of stun server")

	flag.Parse()
}

func main() {
	c, err := stun.Dial(
		protocol,
		addr,
	)

	if err != nil {
		log.Fatal().Err(err).Send()
	}

	message := stun.MustBuild(stun.TransactionID, stun.BindingRequest)

	err = c.Do(message, func(e stun.Event) {
		if e.Error != nil {
			log.Fatal().Err(e.Error).Send()
		}

		var addr stun.XORMappedAddress

		err := addr.GetFrom(e.Message)

		if err != nil {
			log.Fatal().Err(err).Send()
		}

		fmt.Printf("%s:%d\n", addr.IP, addr.Port)
	})

	if err != nil {
		log.Fatal().Err(err).Send()
	}
}
