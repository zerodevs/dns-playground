module gitlab.com/zerodevs/stunc

go 1.15

require (
	github.com/rs/zerolog v1.20.0
	gortc.io/stun v1.23.0
)
