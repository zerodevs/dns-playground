module gitlab.com/zerodevs/dns-playground/ice-test/client

go 1.15

require (
	github.com/pion/ice/v2 v2.0.14
	github.com/pion/randutil v0.1.0
	github.com/r3labs/sse/v2 v2.3.0
	github.com/rs/zerolog v1.20.0
)
