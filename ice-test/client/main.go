package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/r3labs/sse/v2"

	"github.com/pion/ice/v2"
	"github.com/pion/randutil"
	"github.com/rs/zerolog"
)

//nolint
var (
	isServer bool
	agent    *ice.Agent
	server   *sse.Server
	client   *sse.Client
	signaler *sse.Stream
	addr     string
	logger   zerolog.Logger
)

func handleSDP() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			logger.Fatal().Err(err).Send()
		}

		c, err := ice.UnmarshalCandidate(r.PostForm["candidate"][0])

		if err != nil {
			logger.Fatal().Err(err).Send()
		}

		logger.Info().Str("data", r.PostForm.Encode()).Send()

		if err := agent.AddRemoteCandidate(c); err != nil {
			logger.Fatal().Err(err).Send()
		}
	}
}

func init() {
	flag.BoolVar(&isServer, "server", false, "server")
	flag.StringVar(&addr, "addr", "0.0.0.0:9090", "remote addr (as client) or listen addr (as server)")
	flag.Parse()

	logger = zerolog.New(os.Stdout).With().Timestamp().Logger()

	server = sse.New()
	signaler = server.CreateStream("events")

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
}

func main() { //nolint
	var (
		err  error
		conn *ice.Conn
	)

	if !isServer {
		fmt.Println("run as client")
		client = sse.NewClient(fmt.Sprintf("http://%s/events", addr))

		go func() {
			err := client.Subscribe("events", func(msg *sse.Event) {
				logger.Info().Bytes("data", msg.Data).Send()

				candidate, err := ice.UnmarshalCandidate(string(msg.Data))
				if err != nil {
					logger.Error().Err(err).Send()
				}

				err = agent.AddRemoteCandidate(candidate)

				if err != nil {
					logger.Error().Err(err).Send()
				}
			})

			if err != nil {
				logger.Fatal().Err(err).Send()
			}
		}()

	} else {
		fmt.Println("run as server")

		mux := http.NewServeMux()
		mux.HandleFunc("/events", server.HTTPHandler)
		mux.HandleFunc("/signaler", handleSDP())

		go func() {
			if err := http.ListenAndServe(addr, mux); err != nil {
				logger.Fatal().Err(err).Send()
			}
		}()
	}

	agent, err = ice.NewAgent(&ice.AgentConfig{
		Urls: []*ice.URL{
			&ice.URL{
				Scheme: ice.SchemeTypeSTUN,
				Host:   "coturn-server-1.sirogu.com",
				Proto:  ice.ProtoTypeUDP,
				Port:   3478,
			},
		},
		NetworkTypes: []ice.NetworkType{ice.NetworkTypeUDP4},
	})

	if err != nil {
		logger.Fatal().Err(err).Send()
	}

	// When we have gathered a new ICE Candidate send it to the remote peer
	if err = agent.OnCandidate(func(c ice.Candidate) {
		if c == nil {
			return
		}

		if isServer {
			server.Publish("events", &sse.Event{
				Data: []byte(c.Marshal()),
			})
		} else {
			_, err = http.PostForm(fmt.Sprintf("http://%s/signaler", addr), //nolint
				url.Values{
					"candidate": {c.Marshal()},
				})
			if err != nil {
				panic(err)
			}
		}

		logger.Info().Str("local.candidate", c.Marshal()).Send()

	}); err != nil {
		logger.Fatal().Err(err).Send()
	}

	// When ICE Connection state has change print to stdout
	if err = agent.OnConnectionStateChange(func(c ice.ConnectionState) {
		logger.Info().Str("c.state", c.String()).Send()
	}); err != nil {
		logger.Fatal().Err(err).Send()
	}

	// Get the local auth details and send to remote peer
	localUfrag, localPwd, err := agent.GetLocalUserCredentials()

	if err != nil {
		logger.Fatal().Err(err).Send()
	}

	fmt.Printf("%s %s\n", localUfrag, localPwd)
	logger.Info().Str("local.ufrag", localUfrag).Str("local.pwd", localPwd).Send()

	fmt.Print("Press 'Enter' when both processes have started")

	if _, err = bufio.NewReader(os.Stdin).ReadBytes('\n'); err != nil {
		logger.Fatal().Err(err).Send()
	}

	var remoteUfrag, remotePwd string
	reader := bufio.NewReader(os.Stdin)

	fmt.Printf("remote ufrag & remote pwd:\n")
	text, _ := reader.ReadString('\n')

	data := strings.Split(text, " ")

	remoteUfrag = data[0]
	remotePwd = data[1]

	if err = agent.GatherCandidates(); err != nil {
		logger.Fatal().Err(err).Send()
	}

	// Start the ICE Agent. One side must be controlled, and the other must be controlling
	if !isServer {
		conn, err = agent.Dial(context.TODO(), remoteUfrag, remotePwd)
	} else {
		conn, err = agent.Accept(context.TODO(), remoteUfrag, remotePwd)
	}

	if err != nil {
		logger.Fatal().Err(err).Send()
	}

	// Send messages in a loop to the remote peer
	if !isServer {
		for {
			time.Sleep(time.Second * 3)

			val, err := randutil.GenerateCryptoRandomString(15, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
			if err != nil {
				logger.Fatal().Err(err).Send()
			}
			if _, err = conn.Write([]byte(val)); err != nil {
				logger.Fatal().Err(err).Send()
			}

			fmt.Printf("Sent: '%s'\n", val)
		}
	} else {
		// Receive messages in a loop from the remote peer
		buf := make([]byte, 1500)
		for {
			n, err := conn.Read(buf)
			if err != nil {
				logger.Fatal().Err(err).Send()
			}

			fmt.Printf("Received: '%s'\n", string(buf[:n]))
		}
	}
}
